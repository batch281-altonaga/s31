const http = require("http");

// Create a variable port to store port number
const port = 4000;

const app = http.createServer((request, response) => {
  // Accessing the greeting
  if (request.url == "/greeting") {
    response.writeHead(200, { "Content-Type": "text/plain" });
    response.end("Hello Universe!");
  } else if (request.url == "/homepage") {
    response.writeHead(200, { "Content-Type": "text/plain" });
    response.end("Welcome to the homepage!");
  } else {
    response.writeHead(404, { "Content-Type": "text/plain" });
    response.end("404 error! File not found.");
  }
});

app.listen(port);

console.log(`Server is now accessible at localhost:${port}.`);
