// Using require directive to load http module
let http = require("https");

// Create a server
// Send the HTTP header
// HTTP Status: 200 : OK
// What does 200 mean?
// HTTP Status Codes
// 1xx - Informational
// 2xx - Success
// 3xx - Redirection
// 4xx - Client Error
// 5xx - Server Error
// https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
http
  .createServer(function (request, response) {
    response.writeHead(200, { "Content-Type": "text/plain" });
    response.end("Hello World\n");
  })
  .listen(4000);

// 0 - 1023 - well known ports
// 1024 - 49151 - registered ports
// 49152 - 65535 - dynamic ports
// 4000 - 5000

// When server is running, console will print the message:
console.log("Server is running at localhost:4000");

// How to terminate the server
